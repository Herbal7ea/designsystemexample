package com.example.designsystemexample

import android.content.Context
import android.support.annotation.StyleableRes
import android.util.AttributeSet
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView


class CustomView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    @StyleableRes
    internal var authorTextId = 0
    @StyleableRes
    internal var bookTextId = 1
    @StyleableRes
    internal var buttonInfoTextId = 2

    var authorNameTextView: TextView
    var bookTitleTextView: TextView
    var actionButton: Button

    init {
        inflate(context, R.layout.custom_view, this)

        authorNameTextView = findViewById(R.id.author_Text)
        bookTitleTextView = findViewById(R.id.book_Text)
        actionButton = findViewById(R.id.buy_Button)

        attrs?.let { setupFromAttributes(it)  }
    }

    fun setupFromAttributes(attrs: AttributeSet) {
        //Unload values from XML
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.CustomView)

        val author = attributes.getText(authorTextId)
        val book = attributes.getText(bookTextId)
        val buttonText = attributes.getText(buttonInfoTextId)

        attributes.recycle()

        //Apply those values
        authorNameTextView.text = author
        bookTitleTextView.text = book
        actionButton.text = buttonText
    }
}