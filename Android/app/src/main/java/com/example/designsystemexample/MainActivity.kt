package com.example.designsystemexample

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.custom_view.*
import kotlinx.android.synthetic.main.custom_view.view.*

class MainActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val cv = CustomView(this)
            cv.authorNameTextView.text = "Terry Pratchett"
            cv.bookTitleTextView.text = "Going Postal"
            cv.buy_Button.text = "Something Enjoyable"
            cv.buy_Button.setOnClickListener {
                println("🦄 time for a good read!")
            }

        mainLayout.addView(cv)
    }
}
